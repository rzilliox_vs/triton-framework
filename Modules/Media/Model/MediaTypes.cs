﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Triton.Model;
using Triton.Model.Dao;
using Triton.Media.Model;
using Triton.Media.Model.Dao;

namespace Triton.Media.Model
{
    public class MediaTypes: SingletonBase<MediaTypes, MediaType, long>
    {
        private static MediaTypes instance = null;

        private static object syncLock = new object();

        private List<MediaType> mediaTypes;


        public static MediaTypes Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new MediaTypes();
                        }
                    }
                }
                return instance;
            }
        }

        public MediaTypes()
        {
            LoadMediaTypes();
        }


        /// <summary>
        /// Indexer to get a MediaTypes by ID
        /// </summary>
        /// <param name="id">The ID of the MediaTypes to get</param>
        /// <returns>The MediaTypes with the given ID</returns>
        public MediaType this[int id]
        {
            get { return mediaTypes.FirstOrDefault(m => m.Id == id); }
        }

        public List<MediaType> Items { get { return mediaTypes; } } 

        /// <summary>
        /// Loads the mediatypes from the database and populates singleton
        /// </summary>
        private void LoadMediaTypes()
        {
            IMediaTypeDao imtdao = DaoFactory.GetDao<IMediaTypeDao>();

            List<MediaType> results = new List<MediaType>(imtdao.Get(new MediaType()));

            if ((results != null) && (results.Count > 0))
            {
                mediaTypes = results;
            }
        }

    }
}
