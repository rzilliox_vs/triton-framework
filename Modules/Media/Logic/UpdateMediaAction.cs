﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using Common.Logging;
using Triton.Controller;
using Triton.Controller.Action;
using Triton.Controller.Request;
using Triton.Media.Model;
using Triton.Media;
using Triton.Media.Model.Dao;
using Triton.Model.Dao;
using Triton.Web.Support;
using Triton.Media.Model.Dao.Support;
using Triton.Model;
using Triton.Logic.Support;

namespace Triton.Media.Logic
{
    public class updateMediaAction : IAction
    {
        private readonly ILog logger = LogManager.GetCurrentClassLogger();

        private const string EVENT_ERROR = "error";
        private const string EVENT_OK = "ok";

        public updateMediaAction()
        {
            updateMediaActionNameIn = "media";
        }

        public string updateMediaActionNameIn { get; set; }

        public string Execute(TransitionContext context)
        {
            string retEvent = EVENT_ERROR;
            MvcRequest Request = context.Request;
            IMediaDao mdao = DaoFactory.GetDao<IMediaDao>();

            try
            {

                IList<Model.Media> med;
			    if (Request.Items[updateMediaActionNameIn] is SearchResult<Model.Media>)
                {
				    med = ((SearchResult<Model.Media>)Request.Items[updateMediaActionNameIn]).Items;
                    foreach (Model.Media media in med)
                    {
                        mdao.Save(media);
                    }
                    retEvent = EVENT_OK;
			    }
                else if (Request.Items[updateMediaActionNameIn] is Model.Media)
                {
				    med = new List<Model.Media>(new Model.Media[] { (Model.Media)Request.Items[updateMediaActionNameIn] });
                    foreach (Model.Media media in med)
                    {
                        mdao.Save(media);
                    }
                    retEvent = EVENT_OK;
			    }
                else
                {
                    throw new ArgumentException(string.Format("Invalid type in Request.Items[{0}].", updateMediaActionNameIn));
                    retEvent = EVENT_ERROR;
			    }

            }
            catch (Exception ex)
            {
                this.logger.Error("Error occurred in Execute.", ex);
            }

            return retEvent;

        }

    }
}
